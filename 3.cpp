#include<graphics.h>
#include<conio.h>
#pragma comment(lib,"Winmm.lib")
/*******函数********/
void show();
void updatewithoutinput();
void updatewithinput();
void startup();
void gameover();

/************全局变量声明**************/
const int width=350;//宽度
const int  high=600;//高度
const int  bar_num=2;//障碍物个数

//定义所有图片素材
IMAGE img_bk,img_bird1,img_bird2,img_barup1,img_barup2,img_bardown1,img_bardown2;

int bird_x,bird_y;//小鸟坐标
int bar_x[bar_num],bar_upy[bar_num],bar_downy[bar_num];//障碍物坐标
int k;
//主函数
int main()
{
	startup();
	while(1)
	{
		show();
		updatewithoutinput();
		updatewithinput();
	}
	gameover();
	return 0;
}
//*************初始化函数的定义*****************//
void startup()
{
	initgraph(width,high);
	//图片导入
	loadimage(&img_bk,"D:\\flappy bird\\background.jpg");
	loadimage(&img_bird1,"D:\\flappy bird\\bird1.jpg");
	loadimage(&img_bird2,"D:\\flappy bird\\bird2.jpg");
	loadimage(&img_barup1,"D:\\flappy bird\\bar_up1.gif");
	loadimage(&img_barup2,"D:\\flappy bird\\bar_up2.gif");
	loadimage(&img_bardown1,"D:\\flappy bird\\bar_down1.gif");
	loadimage(&img_bardown2,"D:\\flappy bird\\bar_down2.gif");

	bird_x=50;
	bird_y=high/3;

	int i;
	bar_x[0]=width;
	bar_upy[0]=rand()%200-500;
	bar_downy[0]=bar_upy[0]+700;
	//后面的柱子
	for(i=1;i<bar_num;i++)
	{
		bar_x[i]=bar_x[i-1]-250;
		bar_upy[i]=rand()%200-500;
		bar_downy[i]=bar_upy[i]+700;
	}
	mciSendString("open D:\\flappy bird\\backgroundmusic.mp3 alias bkmusic",NULL,0,NULL);
	mciSendString("play bkmusic repeat",NULL,0,NULL);
	BeginBatchDraw();
}

//*****************游戏显示***************//
void show()
{
	putimage(0,0,&img_bk);
	putimage(bird_x,bird_y,&img_bird1,NOTSRCERASE);
	putimage(bird_x,bird_y,&img_bird2,SRCINVERT);
	for(int i=0;i<bar_num;i++)
	{
		putimage(bar_x[i],bar_upy[i],&img_barup1,NOTSRCERASE);
		putimage(bar_x[i],bar_upy[i],&img_barup2,SRCINVERT);

		putimage(bar_x[i],bar_downy[i],&img_bardown1,NOTSRCERASE);
		putimage(bar_x[i],bar_downy[i],&img_bardown2,SRCINVERT);
	}

	FlushBatchDraw();
	
}

//*********************与用户无关的更新****************//
void updatewithoutinput()
{
	bird_y+=5;
	for(int i=0;i<bar_num;i++)
	{		
			if(bar_x[i]>-140 )
			{
				bar_x[i]--;
			}
			else 
			{
				k=rand()%200;
				if(k>100)
				{
				bar_x[i]=width;
				bar_upy[i]=k-500;
				bar_downy[i]=bar_upy[i]+700;
				}
			}
		
	}

	Sleep(50 );
}

//*******************与用户有关的更新**************//
void updatewithinput()
{
	char input;
	if (kbhit())
	{
		input=getch();
		if(input==' ')
		{
			bird_y-=20;
			mciSendString("open D:\\flappy bird\\Jump.mp3 alias jpmusic",NULL,0,NULL);
			mciSendString("play jpmusic",NULL,0,NULL);

		}
	}
}

//************游戏结束******************//
void gameover()
{
}